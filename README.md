# Corewire - Training and Consulting - Demo Application

This is an example project that shows how Go projects can be compiled using Docker. It is part of a hands-on in the [Docker training](https://labs.corewire.de/de/docker/) by [corewire](https://corewire.de/).
